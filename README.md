# wave via ifttt

## Correct(ish) at the time of writing. No guarantees or refunds given.


### 1. Create an Nefit Easy Account

https://www.nefit.nl/consument/landingpages/mijneasy

Register and link your device. You;ll need the Serial Number, Access Code and Password.
The site is in Dutch. I use Google Chrome, which translated automatically and helped immensely!

There is no need to install the Nefit app on your phone. Continue using the regular Wave app.


### 2. Connect your Nefit Easy account to IFTTT

If you don't already have an IFTTT Account, register and then connect your Nefit account here:

https://ifttt.com/nefit_easy/


### 3. Set up the webhooks in IFTTT

Using the maker channel in IFTTT : https://ifttt.com/maker_webhooks

I set up the following three webhook triggerss and connected them to the relevant Nefit actions.

 * Event Name: "heating". --> Set Room Temperature using "{{Value1}}" as the temperature
 * Event Name: "heating_auto". --> Set Auto Mode
 * Event Name: "heating_manual". --> Set Manual Mode

Hit the _Documentation_ link on the [webhooks](https://ifttt.com/maker_webhooks) page to see your key and test your webhooks.

Enter your event name of _heating_ from above and a number in _value1_ to set the temperature.


### 4. Creating a custom Alexa Skill

#### 4.1 Get to the Alexa Skills kit and create a new skill. These links should get you there!

You will need to register/login along the way. You should be able too use an existing Amazon account.

[Alexa Skills Kit](https://developer.amazon.com/alexa)

[Start a Skill](https://developer.amazon.com/alexa-skills-kit)

[Create Skill](https://developer.amazon.com/alexa/console/ask)


Give your skill a name and set it up in the region/language of your choice.
(I chose English UK - other regions may not work?)

Choose the 'Custom' model for your skill

Import the [interactionModel.json](/interactionModel.json) (currently this is drag n drop) and click Build.


Under the 'Endpoint' options - select a type of 'AWS Lambda ARN'
You'll need the Skill Id presented for the next few steps


#### 4.2. Go to AWS Lambda Services and register/login.

[AWS Lambda](https://aws.amazon.com/lambda/)

Again - you should be able to use your existing amazon account.

Creat a new Lambda function from scratch
Give it a name
Select the Node.js 6.10 Runtime
Role: Choose an existing role
Existing role: lambda_basic_execution

In your new function

Select a trigger of 'Alexa Skills Kit'

Paste in the Skill ID of your Skill and click ADD.

Copy & Paste the code from [index.js](/index.js) into the editor tab of your function.

add an Environment Variable called _iftttKey_ and paste in your key from IFTTT.

Hit 'Save'!

Copy the ARN id from the top right of your Lambda Function and go back to your Alexa Skills Kit.

Paste this into the 'Default Region' on the 'Endpoint' tab.

Hit 'Save Endpoints'

### 5. Testing your setup

I thoroughly advise testing things at this point. It's my job.

On the Test tab of the Developer console, Enable your skil for testing.

You should be able to enter the following into the Alexa Simulator and see your thermostat temperature change:

> ask thermostat for 10 degrees

(You can also paste the JSON Input into the Test Events on your Lambda Function to test the code there too.)

### 6. Lets try it out!

Having enabled your skill for testing, it should now show up in the 'Your SKills' section of the Alexa app on your phone and be available from your Alexa device. Try it out with one of the sample phrases:

> "Alexa, ask Thermostat to set the temperature to 21 degrees"

or

> "Alexa, ask Thermostat to set the heating to Auto mode"

You can play around in the Developer console and add more Sample Utterances to change how you trigger the skill.

Or change the speachOutput variables in the code to change how alexa responds.


***

This repo is a work in progress. Options and features in Amazon's developer consoles may change over time.
If you've followed all of the steps above and had a really really good look around yourself and still can't get it to work - drop me an email on _waveifttt[at]iain[dot]xyz_ and I'll see if I can help.