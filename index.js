/**
 
 Mainly stolen from Brian Donohue.
 https://www.pluralsight.com/guides/node-js/amazon-alexa-skill-tutorial
 
*/

'use strict';

// Route the incoming request based on type (LaunchRequest, IntentRequest, etc.)
// The JSON body of the request is provided in the event parameter.

exports.handler = function(event, context) {
    try {
        console.log("event.session.application.applicationId=" + event.session.application.applicationId);

        /**
         * Uncomment this if statement and populate with your skill's application ID to
         * prevent someone else from configuring a skill that sends requests to this function.
         */

        //     if (event.session.application.applicationId !== "amzn1.echo-sdk-ams.app.05aecccb3-1461-48fb-a008-000aaaa0a000") {
        //         context.fail("Invalid Application ID");
        //      }

        if (event.session.new) {
            onSessionStarted({ requestId: event.request.requestId }, event.session);
        }

        if (event.request.type === "LaunchRequest") {
            onLaunch(event.request,
                event.session,
                function callback(sessionAttributes, speechletResponse) {
                    context.succeed(buildResponse(sessionAttributes, speechletResponse));
                });
        } else if (event.request.type === "IntentRequest") {
            onIntent(event.request,
                event.session,
                function callback(sessionAttributes, speechletResponse) {
                    context.succeed(buildResponse(sessionAttributes, speechletResponse));
                });
        } else if (event.request.type === "SessionEndedRequest") {
            onSessionEnded(event.request, event.session);
            context.succeed();
        }
    } catch (e) {
        context.fail("Exception: " + e);
    }
};

/** Called when the session starts. */
function onSessionStarted(sessionStartedRequest, session) {
    console.log("onSessionStarted requestId=" + sessionStartedRequest.requestId +
        ", sessionId=" + session.sessionId);

    // add any session init logic here
}

/** Called when the user invokes the skill without specifying what they want. */
function onLaunch(launchRequest, session, callback) {
    console.log("onLaunch requestId=" + launchRequest.requestId +
        ", sessionId=" + session.sessionId);


    var cardTitle = "Hello!",
        speechOutput = "You can say: set the temperature to 19 degrees using the thermostat.",

    callback(session.attributes,
        buildSpeechletResponse(cardTitle, speechOutput, "", true));
}

/** Called when the user specifies an intent for this skill. */
function onIntent(intentRequest, session, callback) {
    console.log("onIntent requestId=" + intentRequest.requestId +
        ", sessionId=" + session.sessionId);

    var iftttKey = process.env.iftttKey;
    console.log("iftttKey is: " + iftttKey);

    var intentName = intentRequest.intent.name;

    // dispatch custom intents to handlers here

    switch (intentName) {
        case 'setTemperature':
            setTemperature(intentRequest);
            break;
        case 'setMode':
            setMode(intentRequest);
            break;

        default:
            // code
    }


    function setTemperature(intentRequest) {

        getDesiredTemperature(intentRequest, sendTemperatureRequest);

        function getDesiredTemperature(intentRequest, callback) {

            var desiredTemperature = intentRequest.intent.slots.temperature.value;
            callback(desiredTemperature);
        }

        function sendTemperatureRequest(desiredTemperature) {

            console.log("Sending temp: " + desiredTemperature);

            var options = {
                host: 'maker.ifttt.com',
                path: '/trigger/heating/with/key/' + iftttKey,
                method: 'POST',
                headers: { 'Content-Type': 'application/json' }
            };

            var data = '{ "value1" : "' + desiredTemperature + '" }';

            var https = require('https');
            var request = https.request(options, function(response) {
                console.log('STATUS: ' + response.statusCode);
                response.setEncoding('utf8');
                response.on('data', function(chunk) {
                    console.log('BODY: ' + chunk);
                });
            });

            request.on('error', function(e) {
                console.log('problem with request: ' + e.message);
            });

            // write data to request body
            request.write(data, function() {

                var cardTitle = "Setting the temperature";
                var speechOutput = "Setting the temperature to " + desiredTemperature + " degrees.";

                callback(session.attributes,
                    buildSpeechletResponse(cardTitle, speechOutput, "", true));
            });

            request.end();

        }

    }

    function setMode(intentRequest) {

        getDesiredMode(intentRequest, sendModeRequest);


        function getDesiredMode(intentRequest, callback) {
            var desiredMode = intentRequest.intent.slots.mode.value;
            callback(desiredMode);

        }

        function sendModeRequest(desiredMode) {

            console.log("Sending mode: " + desiredMode);

            var options = {
                host: 'maker.ifttt.com',
                path: '/trigger/heating_' + desiredMode + '/with/key/' + iftttKey,
                method: 'POST',
                headers: { 'Content-Type': 'application/json' }
            };

            var https = require('https');
            var request = https.request(options, function(response) {
                console.log('STATUS: ' + response.statusCode);
                response.setEncoding('utf8');
                response.on('data', function(chunk) {
                    console.log('BODY: ' + chunk);
                });
            });

            request.on('error', function(e) {
                console.log('problem with request: ' + e.message);
            });

            var data = '{ }';

            // write data to request body
            request.write(data, function() {

                var cardTitle = "Setting the Heating Mode";
                var speechOutput = "Setting the mode to " + desiredMode;

                callback(session.attributes,
                    buildSpeechletResponse(cardTitle, speechOutput, "", true));
            });

            request.end();
        }
    }

}

/**
 * Called when the user ends the session.
 * Is not called when the skill returns shouldEndSession=true.
 */
function onSessionEnded(sessionEndedRequest, session) {
    console.log("onSessionEnded requestId=" + sessionEndedRequest.requestId +
        ", sessionId=" + session.sessionId);

    // Add any cleanup logic here
}


// ------- Helper functions to build responses -------

function buildSpeechletResponse(title, speechOutput, repromptText, shouldEndSession) {
    return {
        outputSpeech: {
            type: "SSML",
            ssml: "<speak>" + speechOutput + "</speak>"
        },
        card: {
            type: "Simple",
            title: title,
            content: speechOutput
        },
        reprompt: {
            outputSpeech: {
                type: "PlainText",
                text: repromptText
            }
        },
        shouldEndSession: shouldEndSession
    };
}

function buildSpeechletResponseWithoutCard(speechOutput, repromptText, shouldEndSession) {
    return {
        outputSpeech: {
            type: "SSML",
            ssml: "<speak>" + speechOutput + "</speak>"
        },
        reprompt: {
            outputSpeech: {
                type: "PlainText",
                text: repromptText
            }
        },
        shouldEndSession: shouldEndSession
    };
}

function buildResponse(sessionAttributes, speechletResponse) {
    return {
        version: "1.0",
        sessionAttributes: sessionAttributes,
        response: speechletResponse
    };
}